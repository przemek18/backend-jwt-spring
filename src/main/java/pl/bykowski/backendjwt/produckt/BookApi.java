package pl.bykowski.backendjwt.produckt;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
//@CrossOrigin(origins = "localhost:4200")
public class BookApi {

    BookRepo bookRepo;

    public BookApi(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    @GetMapping("/api/books")
    public List<Book> get() {
        return bookRepo.findAll();
    }
}
